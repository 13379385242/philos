/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_3.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/22 20:23:46 by amajid            #+#    #+#             */
/*   Updated: 2024/01/23 16:59:36 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	is_dead_part_0(t_global *g, char *is_exit,
	long long *i, char all_eat_req_num)
{
	if ((*i) == g->philo_num)
	{
		if (all_eat_req_num && g->is_n_eate_times)
			(*is_exit) = 1 * g->is_n_eate_times;
		ft_sleep(5);
		(*i) = 0;
	}
}

void	free_data(t_global *g, t_philo *philos, long long size)
{
	int	j;

	j = -1;
	while (++j < size)
		pthread_mutex_destroy(&g->forks[j]);
	j = -1;
	while (++j < size)
		pthread_mutex_destroy(&g->is_forks_mutex[j]);
	free(g->is_forks_mutex);
	free(g->is_fork);
	free(g->forks);
	free(philos);
	return ;
}
