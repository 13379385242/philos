/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_0.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/15 18:12:38 by amajid            #+#    #+#             */
/*   Updated: 2024/01/22 19:56:31 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	print_msg(t_philo *philo, char *msg, char lock)
{
	struct timeval	time;
	t_global		*g;

	g = philo->g;
	pthread_mutex_lock(&philo->g->print_mutex);
	gettimeofday(&time, NULL);
	printf("%ld %lld %s\n", (time.tv_usec / 1000 + time.tv_sec * 1000)
		- (g->start_time.tv_usec / 1000 + g->start_time.tv_sec * 1000),
		philo->philo_num,
		msg);
	if (!lock)
		pthread_mutex_unlock(&philo->g->print_mutex);
}

void	print_time(t_philo *philo)
{
	struct timeval	time;

	gettimeofday(&time, NULL);
	printf("%lld time = %ld\n", philo->philo_num,
		(((time.tv_sec * 1000) + (time.tv_usec / 1000))
			- ((philo->time_befor_of_last_eat.tv_sec * 1000)
				+ philo->time_befor_of_last_eat.tv_usec / 1000)));
}

long	get_time(t_philo *philo)
{
	struct timeval	time;

	gettimeofday(&time, NULL);
	return (((time.tv_sec * 1000) + (time.tv_usec / 1000))
		- ((philo->time_befor_of_last_eat.tv_sec * 1000)
			+ philo->time_befor_of_last_eat.tv_usec / 1000));
}

void	ft_sleep(long long time_ms)
{
	struct timeval	time;
	long long		time_ll;
	struct timeval	curr_time;
	long long		curr_time_ll;

	gettimeofday(&time, NULL);
	time_ll = time.tv_sec * 1000 + time.tv_usec / 1000;
	gettimeofday(&curr_time, NULL);
	curr_time_ll = curr_time.tv_sec * 1000 + curr_time.tv_usec / 1000;
	while ((curr_time_ll - time_ll) < time_ms)
	{
		usleep(100);
		gettimeofday(&curr_time, NULL);
		curr_time_ll = curr_time.tv_sec * 1000
			+ curr_time.tv_usec / 1000;
	}
}

void	lock_mutex(t_philo *philo, t_global *g, long long index)
{
	pthread_mutex_lock(&g->forks[index]);
	pthread_mutex_lock(&philo->g->is_forks_mutex[index]);
	g->is_fork[index] = 1;
	pthread_mutex_unlock(&philo->g->is_forks_mutex[index]);
	print_msg(philo, "has taken a fork", 0);
}
