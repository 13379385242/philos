/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/15 18:38:52 by amajid            #+#    #+#             */
/*   Updated: 2024/01/26 20:01:45 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

void	part_1(t_philo *philo)
{
	while (!philo->g->is_start)
	{
		ft_sleep(2);
		gettimeofday(&philo->time_befor_of_last_eat, NULL);
	}
	print_msg(philo, "is thinking", 0);
	while (((philo->philo_num % 2 == 0) || philo->philo_num
			== philo->g->philo_num) && !philo->is_start_mod_2)
	{
		philo->is_start_mod_2 = 1;
		ft_sleep(philo->g->time_to_eat - 20);
	}
	if (philo->philo_num == philo->g->philo_num && philo->num_eat == 0
		&& philo->g->philo_num % 2)
		ft_sleep(philo->g->time_to_eat - 20);
}

char	part_0(t_philo *philo, t_global *g)
{
	pthread_mutex_lock(&g->is_exit_mutex);
	if (g->is_exit || (philo->num_eat
			== g->n_times_to_eat && g->is_n_eate_times))
	{
		pthread_mutex_unlock(&g->is_exit_mutex);
		return (-1);
	}
	pthread_mutex_unlock(&g->is_exit_mutex);
	part_1(philo);
	return (1);
}

void	*work(void *p)
{
	t_philo		*philo;
	t_global	*g;
	char		is_fork;

	philo = (t_philo *)p;
	g = philo->g;
	while (1)
	{
		if (part_0(philo, g) == -1)
			return (0);
		pthread_mutex_lock(&g->is_forks_mutex[philo->philo_num - 1]);
		is_fork = g->is_fork[philo->philo_num - 1] == 0;
		pthread_mutex_unlock(&g->is_forks_mutex[philo->philo_num - 1]);
		if (is_fork)
			lock_mutex(philo, g, philo->philo_num - 1);
		else
		{
			if_part_2(philo);
			continue ;
		}
		work_body(philo);
	}
}

void	exit_threads(t_philo *philos, long long size, char is_exit, long long i)
{
	long long	j;

	pthread_mutex_lock(&philos->g->is_exit_mutex);
	philos->g->is_exit = 1;
	pthread_mutex_unlock(&philos->g->is_exit_mutex);
	if (!is_exit)
	{
		j = -1;
		print_msg(&philos[i], "died", 1);
		while (++j < size)
			if (pthread_detach(philos[j].philo))
				printf("Warning: detach returned an error\n");
		ft_sleep(500);
	}
	j = -1;
	while (++j < size)
		pthread_join(philos[j].philo, NULL);
	free_data(philos->g, philos, size);
}

void	is_dead(t_philo *philos, t_global *g, long long size)
{
	long long			i;
	long				time_befor_eate;
	char				all_eat_req_num;
	char				is_exit;

	is_exit = 0;
	i = 0;
	while (1)
	{
		all_eat_req_num = 1;
		time_befor_eate = get_time(philos + i);
		if (time_befor_eate > g->time_to_die || is_exit)
		{
			exit_threads(philos, size, is_exit, i);
			return ;
		}
		pthread_mutex_lock(&g->print_mutex);
		if (philos[i].num_eat < g->n_times_to_eat)
			all_eat_req_num = 0;
		pthread_mutex_unlock(&g->print_mutex);
		i++;
		is_dead_part_0(g, &is_exit, &i, all_eat_req_num);
	}
}
