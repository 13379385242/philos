/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/12 19:41:04 by amajid            #+#    #+#             */
/*   Updated: 2024/01/25 18:45:34 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"
#include <pthread.h>
#include <stdlib.h>

char	parse(int arg_num, char **args, t_global *g)
{
	g->time_to_die = ft_atoi(args[2]);
	g->time_to_eat = ft_atoi(args[3]);
	g->time_to_sleep = ft_atoi(args[4]);
	if (!g->philo_num || !g->time_to_die
		|| !g->time_to_eat || !g->time_to_sleep)
	{
		printf("ERROR: params are incorrect\n");
		return (0);
	}
	if (arg_num == 6)
	{
		g->n_times_to_eat = ft_atoi(args[5]);
		g->is_n_eate_times = 1;
		if (!g->n_times_to_eat)
		{
			printf("ERROR: params are incorrect\n");
			return (0);
		}
	}
	return (1);
}

char	parse_g(int arg_num, char **args, t_global *g)
{
	int	i;

	if (arg_num < 5 || arg_num > 6)
		return (0);
	g->philo_num = ft_atoi(args[1]);
	if (!parse(arg_num, args, g))
		return (0);
	i = 0;
	g->forks = malloc(sizeof(pthread_mutex_t) * g->philo_num);
	g->is_fork = malloc(sizeof(char) * g->philo_num);
	if (!g->forks || !g->is_fork)
		return (0);
	while (i < g->philo_num)
	{
		g->is_fork[i] = 0;
		if (pthread_mutex_init(g->forks + i, 0))
			return (0);
		i++;
	}
	return (1);
}

char	init_mutex(t_global *g)
{
	int	i;

	i = -1;
	if (pthread_mutex_init(&g->is_exit_mutex, NULL))
		return (0);
	if (pthread_mutex_init(&g->print_mutex, NULL))
		return (0);
	g->is_forks_mutex = malloc(sizeof(pthread_mutex_t) * g->philo_num);
	if (!g->is_forks_mutex)
		return (0);
	while (++i < g->philo_num)
		if (pthread_mutex_init(&g->is_forks_mutex[i], NULL))
			return (0);
	return (1);
}

int	main(int arg_num, char **args)
{
	t_global	g;
	int			i;
	t_philo		*philos;

	g = (t_global){0};
	if (!parse_g(arg_num, args, &g) || !init_mutex(&g))
		return (0);
	philos = malloc(sizeof(t_philo) * g.philo_num);
	if (!philos)
		return (0);
	i = -1;
	while (++i < g.philo_num)
	{
		philos[i].philo_num = i + 1;
		philos[i].is_start_mod_2 = 0;
		philos[i].num_eat = 0;
		philos[i].g = &g;
		gettimeofday(&philos[i].time_befor_of_last_eat, NULL);
		if (pthread_create(&philos[i].philo, NULL, &work, &philos[i]))
			return (0);
	}
	ft_sleep(500);
	g.is_start = 1;
	gettimeofday(&g.start_time, NULL);
	is_dead(philos, &g, g.philo_num);
}
