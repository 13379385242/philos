/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/12 18:03:40 by amajid            #+#    #+#             */
/*   Updated: 2024/01/26 17:45:27 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H
# define PHILO_H

# include <pthread.h>
# include <sys/time.h>
# include <pthread.h>
# include <stdlib.h>

# include <stdio.h>
# include <unistd.h>

typedef struct global
{
	long long		philo_num;
	long long		time_to_die;
	long long		time_to_eat;
	long long		time_to_sleep;
	long long		n_times_to_eat;
	char			is_n_eate_times;
	pthread_mutex_t	*forks;
	char			*is_fork;
	pthread_mutex_t	*is_forks_mutex;
	char			is_exit;
	char			is_start;
	struct timeval	start_time;
	pthread_mutex_t	print_mutex;
	pthread_mutex_t	is_exit_mutex;
}	t_global;

typedef struct philo
{
	t_global		*g;
	long long		philo_num;
	pthread_t		philo;
	struct timeval	time_befor_of_last_eat;
	long long		num_eat;
	char			is_start_mod_2;
}	t_philo;

int		ft_atoi(const char *str);
void	print_msg(t_philo *philo, char *msg, char lock);
void	print_time(t_philo *philo);
long	get_time(t_philo *philo);
void	ft_sleep(long long time_ms);
void	lock_mutex(t_philo *philo, t_global *g, long long index);
void	unlock_mutex(t_global *g, long long index);
void	if_part_2(t_philo *philo);
void	work_body(t_philo *philo);
void	*work(void *p);
void	exit_threads(t_philo *philos, long long size,
			char is_exit, long long i);
void	is_dead(t_philo *philos, t_global *g, long long size);
void	is_dead_part_0(t_global *g, char *is_exit,
			long long *i, char all_eat_req_num);
void	free_data(t_global *g, t_philo *philos, long long size);

#endif