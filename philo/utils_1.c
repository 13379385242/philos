/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   UTILS_1.C                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/15 18:20:51 by amajid            #+#    #+#             */
/*   Updated: 2024/01/22 19:57:22 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	unlock_mutex(t_global *g, long long index)
{
	pthread_mutex_unlock(&g->forks[index]);
	pthread_mutex_lock(&g->is_forks_mutex[index]);
	g->is_fork[index] = 0;
	pthread_mutex_unlock(&g->is_forks_mutex[index]);
}

void	if_part_2(t_philo *philo)
{
	lock_mutex(philo, philo->g, philo->philo_num
		* (philo->philo_num != philo->g->philo_num));
	lock_mutex(philo, philo->g, philo->philo_num - 1);
	print_msg(philo, "is eating", 0);
	gettimeofday(&philo->time_befor_of_last_eat, NULL);
	pthread_mutex_lock(&philo->g->print_mutex);
	philo->num_eat++;
	pthread_mutex_unlock(&philo->g->print_mutex);
	ft_sleep(philo->g->time_to_eat);
	unlock_mutex(philo->g, philo->philo_num
		* (philo->philo_num != philo->g->philo_num));
	unlock_mutex(philo->g, philo->philo_num - 1);
	print_msg(philo, "is sleeping", 0);
	ft_sleep(philo->g->time_to_sleep);
}

void	work_body(t_philo *philo)
{
	lock_mutex(philo, philo->g, philo->philo_num
		* (philo->philo_num != philo->g->philo_num));
	print_msg(philo, "is eating", 0);
	gettimeofday(&philo->time_befor_of_last_eat, NULL);
	pthread_mutex_lock(&philo->g->print_mutex);
	philo->num_eat++;
	pthread_mutex_unlock(&philo->g->print_mutex);
	ft_sleep(philo->g->time_to_eat);
	unlock_mutex(philo->g, philo->philo_num
		* (philo->philo_num != philo->g->philo_num));
	unlock_mutex(philo->g, philo->philo_num - 1);
	print_msg(philo, "is sleeping", 0);
	ft_sleep(philo->g->time_to_sleep);
}
