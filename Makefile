SRCS_S = philo/main.c philo/ft_atoi.c philo/utils_0.c philo/utils_1.c philo/utils_2.c philo/utils_3.c
OBJS_S = $(SRCS_S:.c=.o)
SRCS_C = 
OBJS_C = $(SRCS_C:.c=.o)
CC = cc
AR = ar
C_FLAGS = -Wall -Wextra -Werror -g -Ofast# -O3 -g -fsanitize=address
LD_FLAGS := -pthread
SERVER = philo/philo
CLIENT = philo_bonus/philo_bonus
LD_FLAGS_OBJ = 
.PHONY: clean

all: $(SERVER)


clean:
	rm -rf $(OBJS_S) $(OBJS_C) 

fclean: clean
	rm -rf $(SERVER) $(CLIENT)

re: fclean all


%.o: %.c philo/philo.h
	$(CC) $(C_FLAGS) -c -o $@ $< $(LD_FLAGS_OBJ)

$(SERVER):  $(OBJS_S)
	$(CC) $(C_FLAGS) -o  $@ $(OBJS_S)  $(LD_FLAGS) 

$(CLIENT): $(OBJS_C) 
	$(CC) $(C_FLAGS) -o  $@ $(OBJS_C)  $(LD_FLAGS) 




debug: C_FLAGS+= -g
debug: LD_FLAGS+= -fsanitize=thread
debug: LD_FLAGS_OBJ+= -fsanitize=thread
debug:  $(OBJS_S) $(OBJS_C)
debug:  $(SERVER)


